﻿//Load Data in Table when documents is ready  
var mydatatable = null;
$(document).ready(function () {

    loadData();

});

//Load Data function  
function loadData() {
    if (mydatatable != null)
        mydatatable.destroy();

    $.ajax({
        url: "/Contrats/List",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            var html = '';
            $.each(result, function (key, items) {
                html += '<tr>';
                //html += '<td style="width: 1%;">' + item.IdUser + '</td>';
                html += '<td style="text-transform:uppercase">' + items.ref_contrat + '</td>';
                html += '<td style="text-transform:uppercase">' + items.Deb_contrat + '</td>';
                html += '<td style="text-transform:uppercase">' + items.Fin_contrat + '</td>';
              

                html += '<td style="width: 5%;"><button class="btn btn-primary btn-sm glyphicon glyphicon-pencil" onclick="return getbyID(' + items.IDcontrat + ')"></button> </td>';
                html += '<td style="width: 5%;"><button class="btn btn-danger btn-sm glyphicon glyphicon-trash waves-effect waves-light" onclick="Delete(' + items.IDcontrat + ')"></button> </td>';
                html += '</tr>';

            });
            $('.tbody').html(html);

            mydatatable = $("#data_post").DataTable({

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.15/i18n/French.json"
                },
                //dom: 'lBfrtip',
                dom: 'lfr<"table-filter-container">tip',
                initComplete: function (settings) {
                    var api = new $.fn.dataTable.Api(settings);
                    $(".table-filter-container", api.table().container()).append(
                       $("#table-filter").detach().show()
                    );

                    $("#table-filter select").on("change", function () {
                        mydatatable.search(this.value).draw();
                    });
                }

                //buttons: [
                //'copyHtml5', 'csv', 'excel', 'pdf', 'print'
                //]
            });
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });


}

//Add Data Function   
function Add() {

    var res = validate();
    if (res == false) {
        return false;
    }
    var tpObj = {
        IDcontrat: $('#IDcontrat').val(),
        ref_contrat: $('#ref_contrat').val(),
        Deb_contrat: $('#Deb_contrat').val(),
        Fin_contrat: $('#Fin_contrat').val(),
        Id_loc: $('#Id_loc').val(),
        Id_log: $('#Id_log').val(),
        Id_Prop: $('#Id_Prop').val(),
        Description: $('#Description').val(),
        Nature: $('#Nature').val(),
        
    };
    $.ajax({
        url: "/Contrats/Add",
        data: JSON.stringify(tpObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            loadData();
            $('#myModal').modal('hide');
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });



    //console.log("hvhgc " + fp);

}

////Function for getting the Data Based upon Employee ID  
function getbyID(tpID) {
    //$('#userLogin').css('border-color', 'green');
    //$('#userPass').css('border-color', 'green');
    //$('#list_profil').css('border-color', 'green');

    $.ajax({
        url: "/Contrats/getbyID/" + tpID,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#IDcontrat').val(result.IDcontrat);
         
            $('#ref_contrat').val(result.ref_contrat),
             $('#Deb_contrat').val(result.Deb_contrat),
             $('#Fin_contrat').val(result.Fin_contrat),
             $('#Id_loc').val(result.Id_loc),
            $('#Id_log').val(result.Id_log),
             $('#Id_Prop').val(result.Id_Prop),
             $('#Description').val(result.Description),
             $('#Nature').val(result.Nature),

            $('#myModalLabel').html("<h4 class='modal-title'>Modification</h4>");

            $('#fg_validation').css('display', 'block');


            $('#myModal').modal('show');
            $('#btnUpdate').show();
            $('#btnAdd').hide();
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}

//////function for updating employee's record  
//function Update() {
//    var res = validate();
//    if (res == false) {
//        return false;
//    }
//    var tpObj = {
//        ID: $('#ID').val(),
//        Nom_prop: $('#Nom_prop').val(),
//        Prenoms_prop: $('#Prenoms_prop').val(),
//        Num: $('#Num').val(),
//        Email: $('#Email').val(),

//    };
//    $.ajax({
//        url: "/Contrat/Update",
//        data: JSON.stringify(tpObj),
//        type: "POST",
//        contentType: "application/json;charset=utf-8",
//        dataType: "json",
//        success: function (result) {
//            loadData();
//            $('#myModal').modal('hide');
//            $('#ID').val("");
//            $('#Nom_prop').val("");
//            $('#Prenoms_prop').val("");
//            $('#Num').val("");
//            $('#Email').val("")


//        },
//        error: function (errormessage) {
//            alert(errormessage.responseText);
//        }
//    });
//}

////function for deleting employee's record  
function Delete(ID) {

    $("#DeleteModal").modal('show');

    $('.btnvalidersupp').on('click', function () {
        $.ajax({
            url: "/Contrats/Delete/" + ID,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                loadData();
                $("#DeleteModal").modal('hide');
                window.location.reload();
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });

    });
    $('.btnfermer').on('click', function () { $("#DeleteModal").modal('hide'); });
}

//Function for clearing the textboxes  
function clearTextBox() {

    $('#ref_contrat').val(""),
     $('#Deb_contrat').val(""),
   $('#Fin_contrat').val(""),
     $('#Id_loc').val(""),
     $('#Id_log').val(""),
     $('#Id_Prop').val(""),
     $('#Description').val(""),
   $('#Nature').val(""),


    //$('#validation').val("");
    //$('#fg_validation').css('display', 'none');

     $('#myModalLabel').html("<h4 class='modal-title'>Nouvel enregistrement</h4>");
    $('#btnUpdate').hide();
    $('#btnAdd').show();
    //$('#userLogin').css('border-color', 'lightgrey');
    //$('#userPass').css('border-color', 'lightgrey');
    //$('#list_profil').css('border-color', 'lightgrey');

}
//Valdidation using jquery  
function validate() {
    var isValid = true;
    if ($('#ref_contrat').val().trim() == "") {
        $('#ref_contrat').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#ref_contrat').css('border-color', 'lightgrey');

    }

    return isValid;
}








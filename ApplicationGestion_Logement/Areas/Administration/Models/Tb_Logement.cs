﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gestion_log.Models
{
    public class Tb_Logement
    {
        public int IDlog { get; set; }
        public string Titre { get; set; }
        public string Superficie { get; set; }
        public int Id_Quartier { get; set; }
        public string Libelle_quartier { get; set; }
        public int Id_Typelog { get; set; }

        public string Libelle_typelog { get; set; }
        public int Id_statut { get; set; }
        public string Libelle_statut { get; set; }
        public int Prix_location { get; set; }
     
        public string MATlog { get; set; }
        public string charges_forfetaire { get; set; }
       
        public int CordX { get; set; }
        public int CordY { get; set; }
        public string Desclog { get; set; }
        public int Distance { get; set; }
        public string Adrlog { get; set; }
        public bool Is_validate { get; set; }
        public bool Is_Delete { get; set; }
      
      
      


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApplicationGestion_Logement.Areas.Administration.Models
{
    public class Tb_Locataire
    {
        public int IDloc { get; set; }
        public string Nom_loc { get; set; }
        public string Prenom_loc { get; set; }
        public string Naiss_loc { get; set; }
        public string Contact_loc { get; set; }
        public int Id_Profil { get; set; }
        public string lib_Profil { get; set; }

        public string Email { get; set; }
    }
}
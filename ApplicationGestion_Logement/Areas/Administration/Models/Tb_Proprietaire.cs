﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gestion_log.Models
{
    public class Tb_Proprietaire
    {
        public int ID { get; set; }
        public string Nom_prop { get; set; }
        public string Prenoms_prop { get; set; }
        public string Num { get; set; }
        public string Email { get; set; }
        public string DateEnregistrement { get; set; }
    }
}
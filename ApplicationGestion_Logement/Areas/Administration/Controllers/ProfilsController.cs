﻿using ApplicationGestion_Logement.Models;
using Gestion_log.Models;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ApplicationGestion_Logement.Areas.Administration.Controllers
{
    public class ProfilsController : Controller
    {
        private location_bdEntities db = new location_bdEntities();
        // GET: Profils
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult List()
        {
            var prop = db.Profil.Where(e => e.Is_delete == false);
            List<Tb_Profil> items = new List<Tb_Profil>();
            foreach (var item in prop)
            {
                items.Add(new Tb_Profil
                {
                    ID = item.ID,
                    Lib_profil = item.Lib_profil
                });
            }


            return Json(items.ToList(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult Add(Profil item)
        {
            try
            {
                item.Is_delete = false;
                db.Profil.Add(item);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetbyID(int ID)
        {
            var prof = db.Profil.Where(e => e.ID == ID).SingleOrDefault();
            if (prof != null)
            {
                var item = new Tb_Profil
                {
                    ID = prof.ID,
                    Lib_profil = prof.Lib_profil


                };




                return Json(item, JsonRequestBehavior.AllowGet);
            }

            else
                return Json(new { action = false }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Update(Profil item)
        {
            var prof = db.Profil.Where(e => e.ID == item.ID).SingleOrDefault();
            try
            {
                prof.Lib_profil = item.Lib_profil;

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            var prof = db.Profil.Where(e => e.ID == ID).SingleOrDefault();
            try
            {
                prof.Is_delete = true;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }

    }
}
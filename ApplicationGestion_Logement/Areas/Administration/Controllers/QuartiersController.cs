﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Gestion_log.Models;
using ApplicationGestion_Logement.Models;

namespace ApplicationGestion_Logement.Areas.Administration.Controllers
{
    public class QuartiersController : Controller
    {
        private location_bdEntities db = new location_bdEntities();

        // GET: Quartiers
        public async Task<ActionResult> Index()
        {
            var quartiers = db.Quartier.Include(q => q.Commune);
            return View(await quartiers.ToListAsync());
        }

        //CONTROLLER_PRECHARGEMENT LISTE
        [HttpGet]
        [Route("quartiers/Creerquartier", Name = "Creerquartier_route")]
        public JsonResult Creerquartier()
        {
            var communereq = db.Commune.Select(e => new
            {
                idcommune = e.IDcom,
                nom = e.Nom_com
            }).ToList();

            //if (locatairereq != null && logementreq!= null)
            //{

            //}

            //return Json(null, JsonRequestBehavior.AllowGet);
            return Json(new
            {
                list_commune = communereq,


            }, JsonRequestBehavior.AllowGet);
        }
        //FINPRECHARGEMENT

        ////AJOUTQuartier
        [HttpPost]
        [Route("Quartiers/ValiderAjoutquartier", Name = "ValiderAjoutquartier_route")]
        public ActionResult ValiderAjoutquartier(string nomquartier, int? idcommune)
        {          
        if (ModelState.IsValid)
            {               
                    var newquartier = new Quartier { Nom_quartier = nomquartier, Idcommune = idcommune };
                    db.Quartier.Add(newquartier);
                    db.SaveChanges();                          
            }

            return RedirectToAction("Index");
        }
        ////FINAJOUT
     
        
        //CONTROLLER_Modifier
        [HttpGet]
        [Route("Quartiers/Modifierquartier", Name = "Modifierquartier_route")]
        public JsonResult Modifierquartier(int Idquartiermodif)
        {

            var quart = db.Quartier.Where(e => e.IDQuartier == Idquartiermodif).FirstOrDefault();
            if (quart != null)
            {

                return Json(new
                {
                    idmodif = quart.IDQuartier,
                    nomquartier = quart.Nom_quartier,
                    idcommune = quart.Idcommune,
                    //Nomcommune = quart.Commune.Nom_com              
                }, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        //FINMODIFIER

        ////VALIDERAJOUTQUARTIERMODIFIER
        [HttpPost]
        [Route("Quartiers/Validermodificationquartier", Name = "Validermodificationquartier_route")]
        public ActionResult Validermodificationquartier(string nom_quartier, int? idcommune, int Idquartier)
        {
            if (ModelState.IsValid)
            {
                if (ModelState.IsValid)
                {
                    var quartmodif = db.Quartier.Where(e => e.IDQuartier == Idquartier).FirstOrDefault();
                    if (quartmodif != null)
                    {                      
                        quartmodif.Nom_quartier = nom_quartier;
                        quartmodif.Idcommune = idcommune;
                        db.SaveChanges();
                    }

                }
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }
        ////FINAJOUT
        //CONTROLLER_Supprimer
        [HttpPost]
        [Route("Quartiers/Validersuppresionquartier", Name = "Validersuppresionquartier_route")]
        public ActionResult Validersuppresionquartier(int? Idquartiervalidersupp)
        {
            if (ModelState.IsValid)
            {
                var quartsupp = db.Quartier.Where(e => e.IDQuartier == Idquartiervalidersupp.Value).FirstOrDefault();
                if (quartsupp != null)
                {
                    db.Quartier.Remove(quartsupp);
                    db.SaveChanges();
                }

            }
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

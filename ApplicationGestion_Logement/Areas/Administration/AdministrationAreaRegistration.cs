﻿using System.Web.Mvc;

namespace ApplicationGestion_Logement.Areas.Administration
{
    public class AdministrationAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Administration";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Administration_default",
                "Administration/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional, controller = "Home" }
                //namespaces: new[] { "ApplicationGestion_Logement.Areas.Administration.Controllers" }
            );
        }
    }
}